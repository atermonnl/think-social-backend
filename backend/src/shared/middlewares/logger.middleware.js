

/**
 * 
 * @param {import('koa').Context} ctx 
 * @param {import('koa').Next} next 
 */
export async function loggerMiddleware(ctx, next) {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}`);
}