/**
 * Helper function to validate an object against the provided schema,
 * and to throw a custom error if object is not valid.
 *
 * @param {Object} object The object to be validated.
 * @param {String} label The label to use in the error message.
 * @param {import('joi').ObjectSchema & { [key: string]: any | undefined }} schema The Joi schema to validate the object against.
 */
async function validateObject(object = {}, label, schema, options) {
  // Skip validation if no schema is provided
  if (schema) {
    // Validate the object against the provided schema
    const { error, value } = await schema.validateAsync(object, options);
    if (error) {
      // Throw error with custom message if validation failed
      throw new Error(`Invalid ${label} - ${error.message}`);
    }
  }
}

/**
 * Generate a Koa middleware function to validate a request using
 * the provided validation objects.
 *
 * @param {any} validationObj
 * @returns A validation middleware function.
 */
export function validate(validationObj) {
  // Return a Koa middleware function
  return async (ctx, next) => {
    try {
      // Validate each request data object in the Koa context object
      const validationArr = [
        validateObject(ctx.headers, "Headers", validationObj.headers, { allowUnknown: true }),
        validateObject(ctx.params, "URL Parameters", validationObj.params),
        validateObject(ctx.query, "URL Query", validationObj.query)
      ];


      if (ctx.request.body) {
        validationArr.push(validateObject(ctx.request.body, "Request Body", validationObj.body));
      }

      if (ctx.request.files) {
        validationArr.push(validateObject(ctx.request.files, "Request Files", validationObj.files));
      }

      await Promise.all(validationArr);

      return next();
    } catch (err) {
      // If any of the objects fails validation, send an HTTP 400 response.
      ctx.throw(400, err.message);
    }
  };
}
