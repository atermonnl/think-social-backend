import * as nodemailer from "nodemailer";
import { htmlToText } from "nodemailer-html-to-text";


export class NodemailerLib {

  /**
   * @param {Object} param
   * @param {String} param.host - smtp host
   * @param {Number} param.port - smtp port
   * @param {Boolean} param.secure - use tls
   * @param {Object} param.auth
   * @param {String} param.auth.user - smtp account login
   * @param {String} param.auth.pass - smtp account password
   */
  constructor({ host, port, secure, auth }) {
    this.transporter = nodemailer.createTransport({
      host: host,
      port: port,
      secure: secure, // upgrade later with STARTTLS
      auth: {
        user: auth.user,
        pass: auth.pass
      }
    });
    this.transporter.use("compile", htmlToText());
  }

  /**
   * @param {String} from - from email
   * @param {String} to - to email
   * @param {String} html - html letter
   */
  async sendMail(from, to, html) {
    await this.transporter.sendMail({
      from: from,
      to: to,
      html: html
    });
  }
}
