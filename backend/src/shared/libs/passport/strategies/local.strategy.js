import passport from "koa-passport";
import { Strategy as LocalStrategy } from 'passport-local';
import { getManager } from 'typeorm';
import createError from 'http-errors';
import * as bcrypt from 'bcrypt';
import { UserEntity } from '../../../db/entities';

const localStrategyOptions = {
  session: false,
  usernameField: 'email',
  passwordField: 'password'
};

passport.use('client-local', new LocalStrategy(localStrategyOptions, (email, password, done) => {
  const entityManager = getManager();
  const userEntity = entityManager.getRepository(UserEntity);

  userEntity.findOne({ where: { email: email } })
    .then(async userEntity => {
      if (!userEntity) {
        const err = createError(401);
        done(err);
      }

      const passwordIsCorrect = await bcrypt.compare(password, userEntity.passwordHash);
      if (!passwordIsCorrect) {
        const err = createError(401);
        done(err);
      }

      done(null, userEntity);
    })
    .catch(err => done(createError(err, { expose: false })));
}))