import passport from "koa-passport";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import { getManager } from 'typeorm';
import createError from 'http-errors';
import { config } from "../../../configs";
import { UserEntity } from '../../../db/entities';


const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwt.secret
};

passport.use('client-jwt', new JwtStrategy(jwtOptions, (payload, done) => {
  const entityManager = getManager();
  const userRepository = entityManager.getRepository(UserEntity);
  userRepository.findOne({ where: { id: payload.id } }).then(user => {
    if (!user) {
      const err = createError(401, { expose: false });
      done(err);
    }
    done(null, user);
  }).catch(err => {
    done(createError(err));
  });
})
);
