import * as path from "path";
import { readFile } from "fs";
import { promisify } from "util";

import { NodemailerLib } from "../libs/nodemailer";
import { config } from "../configs";


export class EmailSenderService {
  #nodemailer;
  #from;
  #readFilePromisification;

  constructor() {
    this.#nodemailer = new NodemailerLib(config.nodemailer);
    this.#from = config.nodemailer.auth.user;
    this.#readFilePromisification = promisify(readFile);
  }

  /**
   * 
   * @param {String} htmlPath 
   */
  // @ts-ignore
  async #readHtml(htmlPath) {
    const data = await this.#readFilePromisification(htmlPath);
    return data.toString("utf-8");
  }

  /**
   * 
   * @param {String} email 
   * @param {String} changePasswordLink 
   */
  async sendForgotPasswordLetter(email, changePasswordLink) {
    const htmlPath = path.join(global.__projectFolder, "./resources/letters/forgot-password.letter.html");
    let htmlString = await this.#readHtml(htmlPath);

    htmlString = htmlString.replace("#CHANGE_PASSWORD_LINK", changePasswordLink);

    await this.#nodemailer.sendMail(this.#from, email, htmlString);
  }
}
