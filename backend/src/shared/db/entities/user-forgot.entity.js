import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserEntity } from './user.entity';


@Entity('UserForgots')
export class UserForgotEntity {
  @PrimaryGeneratedColumn('uuid')
  id = undefined;

  @Column("varchar")
  code = "";

  @Column("datetime")
  expiresAt = new Date();

  @Column('boolean', { default: true })
  isActive = true;

  @ManyToOne(type => UserEntity, { nullable: false, onDelete: "CASCADE", onUpdate: "CASCADE" })
  user = undefined;
}
