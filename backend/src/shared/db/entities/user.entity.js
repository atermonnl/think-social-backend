import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';


@Entity('Users')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id = undefined;

  @Column("varchar")
  username = "";

  @Column("varchar")
  email = "";

  @Column("text")
  passwordHash = "";

  @Column("varchar")
  avatarId = "";
}
