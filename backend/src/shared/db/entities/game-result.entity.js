import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserEntity } from './user.entity';


@Entity('GameResults')
export class GameResultEntity {
  @PrimaryGeneratedColumn('uuid')
  id = undefined;

  @Column('integer')
  imageScanCount = 0;

  @Column('integer')
  totalImageScanCount = 0;

  @Column('integer')
  totalScore = 0;

  @Column({
    type: 'enum',
    enum: { gold: "gold", silver: "silver", bronze: "bronze", none: 'none' }
  })
  badge = undefined;

  @ManyToOne(type => UserEntity, { nullable: false, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  user = undefined;
}
