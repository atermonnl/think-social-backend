import { EntityRepository, Repository } from 'typeorm';
import { GameResultEntity } from '../entities';


@EntityRepository(GameResultEntity)
export class GameResultRepository extends Repository {

  /**
   * 
   * @param {String} userId 
   */
  async findOneByUserId(userId) {
    return this.createQueryBuilder('GameResults')
      .where('GameResults.userId = :userId', { userId: userId })
      .getOne();
  }

  /**
   * 
   * @param {String} userId 
   */
  async deleteByUserId(userId) {
    await this.createQueryBuilder('GameResults')
      .where('GameResults.userId = :userId', { userId: userId })
      .delete()
      .execute();
  }
}
