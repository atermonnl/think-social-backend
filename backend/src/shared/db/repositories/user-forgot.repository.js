import { EntityRepository, Repository } from 'typeorm';
import { DateTime } from 'luxon';
import { UserForgotEntity } from '../entities';


@EntityRepository(UserForgotEntity)
export class UserForgotRepository extends Repository {

  /**
   * 
   * @param {String} userId 
   * 
   * @returns {Promise<UserForgotEntity | undefined>}
   */
  async getActiveByUserId(userId) {
    const now = DateTime.utc();
    const query = await this.createQueryBuilder('UserForgots')
      .where("UNIX_TIMESTAMP(UserForgots.expiresAt) * 1000 > :now_ts", { now_ts: now.toMillis() })
      .andWhere("UserForgots.userId = :userId", { userId: userId })
      .andWhere("UserForgots.isActive = TRUE");

    console.log(query.getSql());
    const userForgotEntity = query.getOne();

    return userForgotEntity;
  }

  /**
   * 
   * @param {String} code 
   * 
   * @returns {Promise<UserForgotEntity | undefined>}
   */
  async getActiveByCode(code) {
    const now = DateTime.utc();
    const query = await this.createQueryBuilder('UserForgots')
      .innerJoinAndMapOne('UserForgots.user', 'Users', 'Users', 'UserForgots.userId = Users.id')
      .where("UNIX_TIMESTAMP(UserForgots.expiresAt) * 1000 > :now_ts", { now_ts: now.toMillis() })
      .andWhere("UserForgots.code = :code", { code: code })
      .andWhere("UserForgots.isActive = TRUE");

    const userForgotEntity = query.getOne();

    return userForgotEntity;
  }


  /**
   * 
   * @param {String} userId 
   * @param {String} code 
   * 
   * @returns {Promise<UserForgotEntity>}
   */
  async saveCodeWithExpires(userId, code) {
    const expiresAt = DateTime.utc().plus({ minutes: 5 }).toJSDate();

    const newUserForgotEntity = new UserForgotEntity();
    newUserForgotEntity.code = code;
    newUserForgotEntity.user = userId;
    newUserForgotEntity.isActive = true;
    newUserForgotEntity.expiresAt = expiresAt;

    await this.save(newUserForgotEntity);

    return newUserForgotEntity;
  }
}
