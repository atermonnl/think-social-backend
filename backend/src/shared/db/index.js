import { createConnection } from "typeorm";
import "./entities";


export async function createConnectionDb() {
  return createConnection()
    .then(connection => {
      console.log('Db was connected!');
    })
    .catch(error => {
      console.error("Unable connect to Db!");
      console.error("Error: ", error);
      process.exit(1);
    });
}
