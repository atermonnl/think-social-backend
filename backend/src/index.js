import "reflect-metadata";
import "./globals";
import "./shared/configs";
import "./shared/libs/passport";
import "./app";
