import koaMount from "koa-mount";
import koaStatic from "koa-static";
import { config } from "@configs";

/**
 * 
 * @param {import('koa')} app 
 */
export function registerApiDoc(app) {
  if (!config.isProd) {
    app.use(koaMount('/apidoc', koaStatic('apidoc')));
  }
}
