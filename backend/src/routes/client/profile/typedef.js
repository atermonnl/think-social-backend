
/**
  @typedef ChangeProfileRequest
  @property {String} username
  @property {String} email
*/

/**
  @typedef ChangePasswordRequest
  @property {String} oldPassword
  @property {String} newPassword
  */

/**
 @typedef DeleteAccountRequest
 @property {String} password
 */