
export class ProfileController {
  /**
   * 
   * @param {import('./profile.service').ProfileService} profileService 
   */
  constructor(profileService) {
    this.profileService = profileService;
  }

  
  /**
   * 
   * @param {import('koa').DefaultContext} ctx 
   * @param {import('koa').Next} next 
   */
  async getMyProfile(ctx, next) {
    const response = await this.profileService.getMyProfile(ctx.req.user);
    ctx.body = response;
  }

  /**
   * 
   * @param {import('koa').DefaultContext} ctx 
   * @param {import('koa').Next} next 
   */
  async changeMyProfile(ctx, next) {
    const response = await this.profileService.changeMyProfile(ctx.req.user, ctx.request.body);
    ctx.body = response;
  }


  /**
   * 
   * @param {import('koa').DefaultContext} ctx 
   * @param {import('koa').Next} next 
   */
  async changePassword(ctx, next) {
    const response = await this.profileService.changePassword(ctx.req.user, ctx.request.body);
    ctx.body = response;
  }


  /**
   * 
   * @param {import('koa').DefaultContext} ctx 
   * @param {import('koa').Next} next 
   */
  async deleteAccount(ctx, next) {
    const response = await this.profileService.deleteAccount(ctx.req.user, ctx.request.body);
    ctx.body = response;
  }
}
