import { getManager, Not } from 'typeorm';
import createError from "http-errors";
import * as bcrypt from "bcrypt";
import { UserEntity } from '@db/entities';


export class ProfileService {
  constructor() {
    const entityManager = getManager();
    this.userRepository = entityManager.getRepository(UserEntity);
  }


  /**
   * 
   * @param {import('../../../shared/db/entities').UserEntity} userEntity 
   */
  async getMyProfile(userEntity) {
    const plainUser = (({ passwordHash, ...userPlain }) => {
      // @ts-ignore
      return userPlain;
    })(userEntity);
    return plainUser;
  }


  /**
   * 
   * @param {import('../../../shared/db/entities').UserEntity} userEntity 
   * @param {ChangeProfileRequest} changeRequest 
   */
  async changeMyProfile(userEntity, changeRequest) {
    const existUser = await this.userRepository.findOne({ where: [{ username: changeRequest.username, id: Not(userEntity.id) }, { email: changeRequest.email, id: Not(userEntity.id) }] });
    if (existUser) {
      const err = createError(400, "User with this username or email is exists!", { expose: true });
      throw err;
    }

    userEntity.username = changeRequest.username;
    userEntity.email = changeRequest.email;

    await this.userRepository.save(userEntity);

    return { success: true };
  }


  /**
   * 
   * @param {import('../../../shared/db/entities').UserEntity} userEntity 
   * @param {ChangePasswordRequest} changeRequest 
   */
  async changePassword(userEntity, changeRequest) {
    const passwordIsValid = await bcrypt.compare(changeRequest.oldPassword, userEntity.passwordHash);
    if (!passwordIsValid) {
      const err = createError(400);
      throw err;
    }

    userEntity.passwordHash = await bcrypt.hash(changeRequest.newPassword, await bcrypt.genSalt());

    await this.userRepository.save(userEntity);

    return { success: true };
  }


  /**
   * 
   * @param {import('../../../shared/db/entities').UserEntity} userEntity 
   * @param {DeleteAccountRequest} deleteRequest 
   */
  async deleteAccount(userEntity, deleteRequest) {
    const passwordIsValid = await bcrypt.compare(deleteRequest.password, userEntity.passwordHash);
    if (!passwordIsValid) {
      const err = createError(400);
      throw err;
    }

    await this.userRepository.remove(userEntity);

    return { success: true };
  }
}
