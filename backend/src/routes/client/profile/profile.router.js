import Router from 'koa-router';
import passport from 'koa-passport';
import { changeProfileValidator, changePasswordValidator, deletePasswordValidator } from "./validators";
import { ProfileController } from './profile.controller';
import { ProfileService } from './profile.service';


export function createProfileRoutes() {
  const controller = new ProfileController(new ProfileService());
  const router = new Router();

  /**
   * @api {get} /client/profile/my Get my profile
   * @apiName GetMyProfile
   * @apiGroup Profile
   * 
   * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
   *
   * @apiSuccess {Object} response
   * @apiSuccess {String} response.id 
   * @apiSuccess {String} response.username 
   * @apiSuccess {String} response.email 
   * @apiSuccess {String} response.avatarId 
   *
   */
  router.get('/my', passport.authenticate('client-jwt', { session: false }), (ctx, next) => controller.getMyProfile(ctx, next));


  /**
   * @api {post} /client/profile/my/update Change my profile
   * @apiName ChangeMyProfile
   * @apiGroup Profile
   * 
   * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
   * 
   * @apiParam {String} username
   * @apiParam {String} email
   *
   * @apiSuccess {Object} response
   * @apiSuccess {Boolean} response.success
   *
   */
  router.post('/my/update', changeProfileValidator, passport.authenticate('client-jwt', { session: false }), (ctx, next) => controller.changeMyProfile(ctx, next));


  /**
   * @api {post} /client/profile/password/update Change my password
   * @apiName ChangeMyPassword
   * @apiGroup Profile
   *
   * @apiParam {String} oldPassword
   * @apiParam {String} newPassword
   * 
   * @apiSuccess {Object} response
   * @apiSuccess {Boolean} response.success
   *
   */
  router.post('/password/update', passport.authenticate('client-jwt', { session: false }), changePasswordValidator, (ctx, next) => controller.changePassword(ctx, next));


  /**
   * @api {post} /client/profile/my/delete Delete my account
   * @apiName DeleteMyAccount
   * @apiGroup Profile
   * 
   * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
   *
   * @apiParam {String} password
   * 
   * @apiSuccess {Object} response
   * @apiSuccess {Boolean} response.success
   *
   */
  router.post('/my/delete', passport.authenticate('client-jwt', { session: false }), deletePasswordValidator, (ctx, next) => controller.deleteAccount(ctx, next));

  return [router.routes(), router.allowedMethods()];
}
