import Joi from 'joi';
import { validate } from '@validators';


export const changeProfileValidator = validate({
  body: Joi.object({
    username: Joi.string().required(),
    email: Joi.string().email().required()
  })
});

export const changePasswordValidator = validate({
  body: Joi.object({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().required()
  })
});

export const deletePasswordValidator = validate({
  body: Joi.object({
    password: Joi.string().required()
  })
});
