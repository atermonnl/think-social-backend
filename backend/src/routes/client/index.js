import Router from 'koa-router';
import { createAuthRoutes } from './auth/auth.router';
import { createGameResultsRoutes } from './game-results/game-result.router';
import { createProfileRoutes } from './profile/profile.router';


export function createClientRoutes() {
  const router = new Router();

  router.use('/auth', ...createAuthRoutes());
  router.use('/game-results', ...createGameResultsRoutes());
  router.use('/profile', ...createProfileRoutes());

  return [router.routes(), router.allowedMethods()];
}
