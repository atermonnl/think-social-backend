import Joi from 'joi';
import { validate } from '@validators';


export const signUpValidator = validate({
  body: Joi.object({
    username: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    avatarId: Joi.string().required()
  })
});

export const signInValidator = validate({
  body: Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required()
  })
});

export const forgotPasswordValidator = validate({
  query: Joi.object({
    email: Joi.string().email().required()
  })
});

export const forgotPasswordChangeValidator = validate({
  body: Joi.object({
    code: Joi.string().required(),
    newPassword: Joi.string().required()
  })
});
