
/**
  @typedef RegistrationRequest
  @property {String} username
  @property {String} email
  @property {String} password
  @property {String} avatarId
*/

