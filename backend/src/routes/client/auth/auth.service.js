import { getManager } from 'typeorm'
import * as bcrypt from 'bcrypt';
import createError from "http-errors";
import jwt from "jsonwebtoken";
import * as uuid from "uuid";
import { UserEntity } from '@db/entities';
import { config } from '@configs';
import { EmailSenderService } from '@services/email-sender.service';
import { UserForgotRepository } from '@db/repositories';


export class AuthService {
  /**
   * @type {import('typeorm').Repository<UserEntity>}
   */
  #userRepository;
  /**
   * @type {EmailSenderService}
   */
  #emailSenderService;
  /**
   * @type {UserForgotRepository}
   */
  #userForgotRepository;

  constructor() {
    const entityManager = getManager();
    this.#userRepository = entityManager.getRepository(UserEntity);
    this.#userForgotRepository = entityManager.getCustomRepository(UserForgotRepository);
    this.#emailSenderService = new EmailSenderService();
  }

  /**
   * 
   * @param {RegistrationRequest} requestModel 
   */
  async signUp(requestModel) {
    const existUser = await this.#userRepository.findOne({ where: [{ username: requestModel.username }, { email: requestModel.email }] });
    if (existUser) {
      console.log(requestModel.username);
      const err = createError(400, "User with this username or email is exists!", { expose: true });
      throw err;
    }

    const passwordHash = await bcrypt.hash(requestModel.password, await bcrypt.genSalt());

    const newUser = new UserEntity();
    newUser.username = requestModel.username;
    newUser.email = requestModel.email;
    newUser.passwordHash = passwordHash;
    newUser.avatarId = requestModel.avatarId;

    await this.#userRepository.save(newUser);

    const payload = {
      id: newUser.id,
      username: newUser.username,
      email: newUser.email
    };
    const token = jwt.sign(payload, config.jwt.secret);

    return { token: token };
  }


  /**
   * 
   * @param {import('../../../shared/db/entities').UserEntity} userEntity 
   */
  async signIn(userEntity) {
    const payload = {
      id: userEntity.id,
      username: userEntity.username,
      email: userEntity.email
    };
    const token = jwt.sign(payload, config.jwt.secret);
    
    return { token: token };
  }


  /**
   * 
   * @param {String} email 
   * @param {(code: String) => String} urlGenerator 
   */
  async forgotPassword(email, urlGenerator) {
    const userWithEmail = await this.#userRepository.findOne({ where: { email: email } });
    if (!userWithEmail) {
      throw createError(400, "User was not found!", { expose: true });
    }

    const existUserForgotEntity = await this.#userForgotRepository.getActiveByUserId(userWithEmail.id);
    if (existUserForgotEntity) {
      throw createError(400, "User forgot request is exists already!");
    }

    const code = uuid.v4();
    const changePasswordLink = urlGenerator(code);

    await this.#userForgotRepository.saveCodeWithExpires(userWithEmail.id, code);
    await this.#emailSenderService.sendForgotPasswordLetter(email, changePasswordLink);

    return { success: true };
  }


  /**
   * 
   * @param {String} code 
   * @param {String} newPassword 
   */
  async forgotPasswordChange(code, newPassword) {
    const existUserForgotEntity = await this.#userForgotRepository.getActiveByCode(code);
    if (!existUserForgotEntity) {
      throw createError(400, "Forgot request was not found!");
    }

    const userEntity = await this.#userRepository.findOne({ where: { id: existUserForgotEntity.user.id } });
    const passwordHash = await bcrypt.hash(newPassword, await bcrypt.genSalt());
    userEntity.passwordHash = passwordHash;
    await this.#userRepository.save(userEntity);

    existUserForgotEntity.isActive = false;
    await this.#userForgotRepository.save(existUserForgotEntity);

    return { success: true };
  }
}
