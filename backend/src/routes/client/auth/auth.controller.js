import url from 'url';


export class AuthController {
  #authService;

  /**
   * 
   * @param {import('./auth.service').AuthService} authService 
   */
  constructor(authService) {
    this.#authService = authService;
  }

  /**
   * 
   * @param {import('koa').ParameterizedContext} ctx 
   * @param {import('koa').Next} next 
   */
  async signUp(ctx, next) {
    const responseViewModel = await this.#authService.signUp(ctx.request.body);
    ctx.body = responseViewModel;
  }

  /**
   * 
   * @param {import('koa').ParameterizedContext & { req: { [key: string]: any } } } ctx 
   * @param {import('koa').Next} next 
   */
  async signIn(ctx, next) {
    const responseViewModel = await this.#authService.signIn(ctx.req.user);
    ctx.body = responseViewModel;
  }

  /**
   * 
   * @param {import('koa').ParameterizedContext} ctx 
   * @param {import('koa').Next} next 
   */
  async forgotPassword(ctx, next) {
    const { email } = ctx.request.query;
    const response = await this.#authService.forgotPassword(email, code => {
      const newUrl = new url.URL(`http://${ctx.req.headers['host']}/transporters/forgot-password.transporter.html?code=${code}`);
      return newUrl.toString();
    });

    ctx.body = response;
  }


  /**
   * 
   * @param {import('koa').ParameterizedContext} ctx 
   * @param {import('koa').Next} next 
   */
  async forgotPasswordChange(ctx, next) {
    const { code, newPassword } = ctx.request.body;
    const response = await this.#authService.forgotPasswordChange(code, newPassword);
    ctx.body = response;
  }

}
