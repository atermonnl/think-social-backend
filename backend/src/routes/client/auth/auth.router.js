import Router from "koa-router";
import passport from "koa-passport";
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { signUpValidator, signInValidator, forgotPasswordValidator, forgotPasswordChangeValidator } from './validators';


export function createAuthRoutes() {
  const controller = new AuthController(new AuthService());
  const router = new Router();

  /**
   * @api {post} /client/auth/sign-up Sign Up
   * @apiName SignUp
   * @apiGroup Auth
   *
   * @apiParam (Body) {String} username
   * @apiParam (Body) {String} email
   * @apiParam (Body) {String} password
   * @apiParam (Body) {String} avatarId
   *
   * @apiSuccess {Object} response
   * @apiSuccess {String} response.token 
   *
   */
  router.post('/sign-up', signUpValidator, (context, next) => controller.signUp(context, next));


  /**
   * @api {post} /client/auth/sign-ip Sign In
   * @apiName SignIn
   * @apiGroup Auth
   *
   * @apiParam (Body) {String} email
   * @apiParam (Body) {String} password
   *
   * @apiSuccess {Object} response
   * @apiSuccess {String} response.token 
   *
   */
  router.post('/sign-in', signInValidator, passport.authenticate('client-local', { session: false }), (ctx, next) => controller.signIn(ctx, next));


  /**
   * @api {post} /client/auth/forgot-password Forgot Password
   * @apiName ForgotPassword
   * @apiGroup Auth
   *
   * @apiParam (Query) {String} email
   *
   * @apiSuccess {Object} response
   * @apiSuccess {Boolean} response.success 
   *
   */
  router.get('/forgot-password', forgotPasswordValidator, (ctx, next) => controller.forgotPassword(ctx, next));

  /**
   * @api {post} /client/auth/forgot-password/change Change password by forgot-code
   * @apiName ForgotPasswordChange
   * @apiGroup Auth
   *
   * @apiParam (Body) {String} code
   * @apiParam (Body) {String} newPassword
   *
   * @apiSuccess {Object} response
   * @apiSuccess {Boolean} response.success 
   *
   */
  router.post('/forgot-password/change', forgotPasswordChangeValidator, (ctx, next) => controller.forgotPasswordChange(ctx, next));


  return [router.routes(), router.allowedMethods()];
}
