import Router from 'koa-router';
import passport from "koa-passport";
import { saveGameResultValidator } from "./validators";
import { GameResultController } from './game-result.controller';
import { GameResultService } from './game-result.service';


export function createGameResultsRoutes() {
  const controller = new GameResultController(new GameResultService());
  const router = new Router();


  /**
   * @api {get} /client/game-results/last Get my profile
   * @apiName GetLastGameResult
   * @apiGroup GameResults
   * 
   * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
   *
   * @apiSuccess {Object} response
   * @apiSuccess {String} response.id 
   * @apiSuccess {Number} response.imageScanCount 
   * @apiSuccess {Number} response.totalImageScanCount 
   * @apiSuccess {Number} response.totalScore 
   * @apiSuccess {String} response.badge ['silver', 'gold', 'bronze'] 
   *
   */
  router.get("/last", passport.authenticate('client-jwt', { session: false }), (ctx, next) => controller.getLastGameResult(ctx, next));


  /**
   * @api {put} /client/game-results/ Save game result
   * @apiName SaveGameResult
   * @apiGroup GameResults
   * 
   * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
   * 
   * @apiParam (Body) {Number} imageScanCount
   * @apiParam (Body) {Number} totalImageScanCount
   * @apiParam (Body) {Number} totalScore
   * @apiParam (Body) {String} badge ['silver', 'gold', 'bronze'] 
   *
   * @apiSuccess {Object} response
   * @apiSuccess {Boolean} response.success  
   *
   */
  router.put("/", saveGameResultValidator, passport.authenticate('client-jwt', { session: false }), (ctx, next) => controller.saveGameResult(ctx, next));

  return [router.routes(), router.allowedMethods()];
}
