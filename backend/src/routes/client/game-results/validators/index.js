import Joi from 'joi';
import { validate } from '@validators';


export const saveGameResultValidator = validate({
  body: Joi.object({
    imageScanCount: Joi.number().required(),
    totalImageScanCount: Joi.number().required(),
    totalScore: Joi.number().required(),
    badge: Joi.string().valid('silver', 'gold', 'bronze', 'none').required()
  })
});
