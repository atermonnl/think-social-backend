

export class GameResultController {
  /**
   * 
   * @param {import('./game-result.service').GameResultService} gameResultService 
   */
  constructor(gameResultService) {
    this.gameResultService = gameResultService;
  }


  /**
   * 
   * @param {import('koa').DefaultContext} ctx 
   * @param {import('koa').Next} next 
   */
  async getLastGameResult(ctx, next) {
    const responseViewModel = await this.gameResultService.getLastGameResult(ctx.req.user);
    ctx.body = responseViewModel;
  }

  /**
   * 
   * @param {import('koa').DefaultContext} ctx 
   * @param {import('koa').Next} next 
   */
  async saveGameResult(ctx, next) {
    const response = await this.gameResultService.saveGameResult(ctx.req.user, ctx.request.body);
    ctx.body = response;
  }
}
