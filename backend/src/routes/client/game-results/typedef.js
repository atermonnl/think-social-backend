
/**
  @typedef SaveGameResultRequest
  @property {Number} imageScanCount
  @property {Number} totalImageScanCount
  @property {Number} totalScore
  @property {'silver' | 'gold' | 'bronze'} badge
*/
