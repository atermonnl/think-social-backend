import { getManager } from 'typeorm'
import { GameResultEntity } from '@db/entities';
import { GameResultRepository } from '@db/repositories';


export class GameResultService {

  constructor() {
    const entityManager = getManager()
    this.gameResultRepository = entityManager.getCustomRepository(GameResultRepository);
  }

  /**
  * 
  * @param {import('../../../shared/db/entities').UserEntity} userEntity 
  */
  async getLastGameResult(userEntity) {
    const completedGame = await this.gameResultRepository.findOneByUserId(userEntity.id);
    return completedGame;
  }

  /**
   * 
   * @param {import('../../../shared/db/entities').UserEntity} userEntity 
   * @param {SaveGameResultRequest} saveGameRequest 
   */
  async saveGameResult(userEntity, saveGameRequest) {
    await this.gameResultRepository.deleteByUserId(userEntity.id);

    const newGameResultEntity = new GameResultEntity();
    newGameResultEntity.imageScanCount = saveGameRequest.imageScanCount;
    newGameResultEntity.totalImageScanCount = saveGameRequest.totalImageScanCount;
    newGameResultEntity.totalScore = saveGameRequest.totalScore;
    newGameResultEntity.badge = saveGameRequest.badge;
    newGameResultEntity.user = userEntity;

    await this.gameResultRepository.save(newGameResultEntity);
    return { success: true };
  }
}
