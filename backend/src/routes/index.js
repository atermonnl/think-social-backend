import Router from 'koa-router';
import { createClientRoutes } from './client';

export function createRootRoutes() {
  const router = new Router();

  router.use('/client', ...createClientRoutes());

  return [router.routes(), router.allowedMethods()];
}
