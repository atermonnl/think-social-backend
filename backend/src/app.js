import Koa from "koa";
import koaBody from "koa-body";
import passport from "koa-passport";
import koaMount from "koa-mount";
import koaStatic from "koa-static";
import { createRootRoutes } from "./routes";
import { errorHandlerMiddleware } from './shared/middlewares/error-handler.middleware';
import { loggerMiddleware } from './shared/middlewares/logger.middleware';
import { createConnectionDb } from './shared/db';
import { registerApiDoc } from './apidoc';


async function main() {
  await createConnectionDb();

  const app = new Koa();

  app.use(koaBody({ multipart: true }));

  app.use(loggerMiddleware);
  app.use(errorHandlerMiddleware);

  registerApiDoc(app);

  app.use(koaMount('/', koaStatic('public')));

  app.use(passport.initialize());

  // @ts-ignore
  app.use(...createRootRoutes());


  app.on("error", (err, ctx) => {
    console.error(err.stack);
    /* centralized error handling:
    *   console.log error
    *   write error to log file
    *   save error and request information to database if ctx.request match condition
    *   ...
    */
  });


  app.listen(3000, '0.0.0.0', () => { console.log("Server start on http://0.0.0.0:3000"); });
}

main();
